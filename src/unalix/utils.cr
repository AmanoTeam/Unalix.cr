module Unalix

    # The unreserved URI characters (RFC 3986)
    UNRESERVED_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
        "abcdefghijklmnopqrstuvwxyz" +
        "0123456789"

    SAFE_WITH_PERCENT = "!#$%&'()*+,/:;=?@[]~"
    SAFE_WITHOUT_PERCENT = "!#$&'()*+,/:;=?@[]~"

    ALPHANUMERIC_RE = /[a-zA-Z0-9]/

    # Un-escape any percent-escape sequences in a URI that are unreserved
    # characters. This leaves all reserved, illegal and non-ASCII bytes encoded.
    def self.unquote_unreserved(uri : String) : String

        hexadecimal_string : String
        hexadecimal_int : Int32
        character : Char

        parts : Array(String) = uri.split(separator: '%')

        (1 ... parts.size).each do |index|
            hexadecimal_string = parts[index][0 .. 1]

            if hexadecimal_string.size == 2 && ALPHANUMERIC_RE.match(str: hexadecimal_string[0 ... 1])
                hexadecimal_int = hexadecimal_string.to_i(base: 16, whitespace: false)
                character = hexadecimal_int.chr()
                
                if UNRESERVED_CHARACTERS.includes?(search: character)
                    parts[index] = character + parts[index][2 .. -1]
                else
                    parts[index] = '%' + parts[index]
                end
            else
                parts[index] = '%' + parts[index]
            end
        end
        return parts.join(separator: "")
    end

    # This function passes the given URI through an unquote/quote cycle to
    # ensure that it is fully and consistently quoted.
    def self.requote_uri(uri : String) : String
        quoted_uri : String = ""

        begin
            # Unquote only the unreserved characters
            # Then quote only illegal characters (do not quote reserved,
            # unreserved, or '%')
            unquote_unreserved(uri).each_char do |character|
                if (UNRESERVED_CHARACTERS + SAFE_WITH_PERCENT).includes?(search: character)
                    quoted_uri += character
                else
                    quoted_uri += URI.encode(character.to_s)
                end
            end
        rescue
            # We couldn't unquote the given URI, so let"s try quoting it, but
            # there may be unquoted "%"s in the URI. We need to make sure they're
            # properly quoted so they do not cause issues elsewhere.
            uri.each_char do |character|
                if (UNRESERVED_CHARACTERS + SAFE_WITHOUT_PERCENT).includes?(search: character)
                    quoted_uri += character
                else
                    quoted_uri += URI.encode(character.to_s)
                end
            end
        end

        return quoted_uri
    end

    def self.filter_query(query : String, stripEmpty : Bool = false, stripDuplicates : Bool = false) : String

        params : Array(String) = Array(String).new
        names : Array(String) = Array(String).new

        key : String
        value : String
        values : Array(String)
        parts : Array(String)

        query.split(separator: "&", remove_empty: true).each do |param|

            parts = param.split(separator: "=")

            key = parts[0]
            values = Array(String).new

            (1 ... parts.size).each do |index|
                values.push(value: parts[index])
            end

            value = values.join(separator: "%3D")
            value = value.gsub(string: "?", replacement: "%3F")

            # Ignore field with empty value
            if stripEmpty && value == ""
                next
            end

            # Ignore field with duplicate name
            if stripDuplicates && names.includes?(obj: key)
                next
            end

            params.push(value: "#{key}=#{value}")

            names.push(value: key)
        end

        return params.join(separator: "&")
    end

end