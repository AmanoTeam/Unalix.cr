Unalix is a library written in Crystal, it follows the same specification used by the [ClearURLs](https://github.com/ClearURLs/Addon) addon for removing tracking fields from URLs.

#### Installation

Add to your `shard.yml`:

```yaml
dependencies:
  unalix:
    github: AmanoTeam/Unalix.cr
    branch: master
```

_**Note**: Unalix requires Crystal 0.30.0 or higher._

#### Usage:

Code:

```crystal
require "unalix"

url : String = "https://deezer.com/track/891177062?utm_source=deezer"
result : String = Unalix.clear_url(url: url)

puts result
```

Output:

```
https://deezer.com/track/891177062
```

#### Contributing

If you have discovered a bug in this library and know how to fix it, fork this repository and open a Pull Request.

#### Third party software

Unalix includes some third party software in its codebase. See them below:

- **ClearURLs**
  - Author: Kevin Röbert
  - Repository: [ClearURLs/Rules](https://github.com/ClearURLs/Rules)
  - License: [GNU Lesser General Public License v3.0](https://gitlab.com/ClearURLs/Rules/blob/master/LICENSE)

- **Requests**
  - Author: Kenneth Reitz
  - Repository: [psf/requests](https://github.com/psf/requests)
  - License: [Apache v2.0](https://github.com/psf/requests/blob/master/LICENSE)
