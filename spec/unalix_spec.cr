require "./spec_helper"

describe Unalix do
    # TODO: Write tests

    unmodified_url : String

    it "works" do
        unmodified_url = "https://deezer.com/track/891177062?utm_source=deezer"

        Unalix.clear_url(url: unmodified_url).should eq("https://deezer.com/track/891177062")
        Unalix.clear_url(url: unmodified_url, ignoreRules: true).should eq(unmodified_url)

        unmodified_url = "https://www.google.com/url?q=https://pypi.org/project/Unalix"

        Unalix.clear_url(url: unmodified_url).should eq("https://pypi.org/project/Unalix")
        Unalix.clear_url(url: unmodified_url, ignoreRedirections: true).should eq(unmodified_url)

        unmodified_url = "https://www.google.com/amp/s/de.statista.com/infografik/amp/22496/anzahl-der-gesamten-positiven-corona-tests-und-positivenrate/"
        Unalix.clear_url(url: unmodified_url).should eq("http://de.statista.com/infografik/amp/22496/anzahl-der-gesamten-positiven-corona-tests-und-positivenrate/")

        unmodified_url = "http://www.shareasale.com/r.cfm?u=1384175&b=866986&m=65886&afftrack=&urllink=www.rightstufanime.com%2Fsearch%3Fkeywords%3DSo%20I%27m%20a%20Spider%20So%20What%3F"
        Unalix.clear_url(url: unmodified_url).should eq("http://www.rightstufanime.com/search?keywords=So%20I'm%20a%20Spider%20So%20What%3F")

        unmodified_url = "https://www.amazon.com/gp/B08CH7RHDP/ref=as_li_ss_tl"

        Unalix.clear_url(url: unmodified_url).should eq("https://www.amazon.com/gp/B08CH7RHDP")
        Unalix.clear_url(url: unmodified_url, ignoreRawRules: true).should eq(unmodified_url)

        unmodified_url = "https://natura.com.br/p/2458?consultoria=promotop"

        Unalix.clear_url(url: unmodified_url).should eq("https://natura.com.br/p/2458")
        Unalix.clear_url(url: unmodified_url, ignoreReferralMarketing: true).should eq(unmodified_url)

        unmodified_url = "https://myaccount.google.com/?utm_source=google"

        Unalix.clear_url(url: unmodified_url).should eq(unmodified_url)
        Unalix.clear_url(url: unmodified_url, ignoreExceptions: true).should eq("https://myaccount.google.com/")

        unmodified_url = "http://clickserve.dartsearch.net/link/click?ds_dest_url=http://g.co/"

        Unalix.clear_url(url: unmodified_url).should eq("http://g.co/")
        Unalix.clear_url(url: unmodified_url, skipBlocked: true).should eq(unmodified_url)

        unmodified_url = "http://example.com/?p1=&p2="

        Unalix.clear_url(url: unmodified_url).should eq(unmodified_url)
        Unalix.clear_url(url: unmodified_url, stripEmpty: true).should eq("http://example.com/")

        unmodified_url = "http://example.com/?p1=value&p1=othervalue"

        Unalix.clear_url(url: unmodified_url).should eq(unmodified_url)
        Unalix.clear_url(url: unmodified_url, stripDuplicates: true).should eq("http://example.com/?p1=value")

        unmodified_url = "http://example.com/?&&&&"

        Unalix.clear_url(url: unmodified_url).should eq("http://example.com/")
    end
end
