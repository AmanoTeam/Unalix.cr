import json
import http.client
import urllib.parse

sources = [
    {
        "url": "https://rules1.clearurls.xyz/data/data.minify.json",
        "path": "src/unalix/rulesets/data.min.json"
    },
    {
        "url": "https://raw.githubusercontent.com/AmanoTeam/Unalix/master/unalix/package_data/rulesets/unalix.json",
        "path": "src/unalix/rulesets/unalix.json"
    }
]

for source in sources:
    url = urllib.parse.urlparse(source.get("url"))

    connection = http.client.HTTPSConnection(
        host=url.netloc,
        port=url.port,
        timeout=7
    )

    print(f"Fetching data from {source.get('url')}...")

    connection.request(
        method="GET",
        url=url.path,
    )
    response = connection.getresponse()

    content = response.read()
    connection.close()

    with open(file=source.get("path"), mode="w", encoding="utf-8") as file:
        file.write(json.dumps(json.loads(content), indent=4, ensure_ascii=False))
