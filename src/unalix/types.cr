module Unalix

    struct Ruleset

        property providerName
        property urlPattern
        property completeProvider
        property rules
        property rawRules
        property referralMarketing
        property exceptions
        property redirections
        property forceRedirection

        def initialize(
            @providerName : String,
            @urlPattern : Regex,
            @completeProvider : Bool,
            @rules : Array(Regex),
            @rawRules : Array(Regex),
            @referralMarketing : Array(Regex),
            @exceptions : Array(Regex),
            @redirections : Array(Regex),
            @forceRedirection : Bool
        )
        end

    end

end
