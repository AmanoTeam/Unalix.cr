module Unalix

    RULESETS_PATH = [
        Path[__DIR__] / "rulesets/data.min.json",
        Path[__DIR__] / "rulesets/unalix.json"
    ]

    IGNORED_PROVIDERS = [
        "ClearURLsTest",
        "ClearURLsTestBlock",
        "ClearURLsTest2",
        "ClearURLsTestBlock2"
    ]

end
