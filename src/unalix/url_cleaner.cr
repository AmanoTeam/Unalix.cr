require "json"
require "uri"

require "./config"
require "./types"
require "./utils"

module Unalix

    def self.load_rulesets : Array(Unalix::Ruleset)

        urlPattern: Regex
        completeProvider: Bool
        rules: Array(Regex)
        rawRules: Array(Regex)
        referralMarketing: Array(Regex)
        exceptions: Array(Regex)
        redirections: Array(Regex)
        forceRedirection: Bool

        ruleset_hash: Hash(String, JSON::Any)
        rulesets_hash: Hash(String, JSON::Any)

        value = (JSON::Any | Nil)

        rulesets = Array(Unalix::Ruleset).new

        RULESETS_PATH.each do |ruleset_file|
            json: JSON::Any = File.open(ruleset_file) do |json_file|
                JSON.parse(json_file)
            end

            rulesets_hash = json["providers"].as_h

            rulesets_hash.each_key do |providerName|
                ruleset_hash = rulesets_hash[providerName].as_h

                if IGNORED_PROVIDERS.includes?(obj: providerName)
                    next
                end

                # https://docs.clearurls.xyz/latest/specs/rules/#urlpattern
                value = ruleset_hash.fetch(key: "urlPattern", default: nil)

                if value.nil?
                    next
                end

                urlPattern = /#{value.as_s}/

                # https://docs.clearurls.xyz/latest/specs/rules/#completeprovider
                value = ruleset_hash.fetch(key: "completeProvider", default: nil)

                if value.nil?
                    completeProvider = false
                else
                    completeProvider = value.as_bool
                end

                # https://docs.clearurls.xyz/latest/specs/rules/#rules
                value = ruleset_hash.fetch(key: "rules", default: nil)

                rules = Array(Regex).new

                unless value.nil?
                    value.as_a.each do |rule|
                        rules.push(value: /#{"(%(?:26|23)|&|^)" + rule.as_s + "(?:(?:=|%3[Dd])[^&]*)"}/)
                    end
                end

                # https://docs.clearurls.xyz/latest/specs/rules/#rawrules
                value = ruleset_hash.fetch(key: "rawRules", default: nil)

                rawRules = Array(Regex).new

                unless value.nil?
                    value.as_a.each do |rawRule|
                        rawRules.push(value: /#{rawRule}/)
                    end
                end

                # https://docs.clearurls.xyz/latest/specs/rules/#referralmarketing
                value = ruleset_hash.fetch(key: "referralMarketing", default: nil)

                referralMarketing = Array(Regex).new

                unless value.nil?
                    value.as_a.each do |referral|
                        referralMarketing.push(value: /#{"(%(?:26|23)|&|^)" + referral.as_s + "(?:(?:=|%3[Dd])[^&]*)"}/)
                    end
                end

                # https://docs.clearurls.xyz/latest/specs/rules/#exceptions
                value = ruleset_hash.fetch(key: "exceptions", default: nil)

                exceptions = Array(Regex).new

                unless value.nil?
                    value.as_a.each do |exception|
                        exceptions.push(value: /#{exception.as_s}/)
                    end
                end

                # https://docs.clearurls.xyz/latest/specs/rules/#redirections
                value = ruleset_hash.fetch(key: "redirections", default: nil)

                redirections = Array(Regex).new

                unless value.nil?
                    value.as_a.each do |redirection|
                        redirections.push(value: /#{redirection.as_s + ".*"}/)
                    end
                end

                # This field is ignored by Unalix, we are leaving it here just as reference
                # https://docs.clearurls.xyz/latest/specs/rules/#forceredirection
                value = ruleset_hash.fetch(key: "forceRedirection", default: nil)

                if value.nil?
                    forceRedirection = false
                else
                    forceRedirection = value.as_bool
                end

                rulesets.push(Unalix::Ruleset.new(
                    providerName: providerName, # This field is ignored by Unalix
                    urlPattern: urlPattern,
                    completeProvider: completeProvider,
                    rules: rules,
                    rawRules: rawRules,
                    referralMarketing: referralMarketing,
                    exceptions: exceptions,
                    redirections: redirections,
                    forceRedirection: forceRedirection
                ))
            end
        end

        return rulesets
    end

    RULESETS = Unalix.load_rulesets()

    def self.clear_url(
        url : (String | URI),
        ignoreReferralMarketing : Bool = false,
        ignoreRules : Bool = false,
        ignoreExceptions : Bool = false,
        ignoreRawRules : Bool = false,
        ignoreRedirections : Bool = false,
        skipBlocked : Bool = false,
        stripDuplicates : Bool = false,
        stripEmpty : Bool = false
    ) : String
        #
        #    This method implements the same specification used in the addon version of ClearURLs (with a few minor exceptions)
        #    for removing tracking fields and unshortening URLs.
        #
        #    Parameters:
        #
        #        url (`String` | `URI`):
        #            A `String` or `URI` object representing an HTTP URL.
        #
        #        ignoreReferralMarketing (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to not remove referral marketing fields from the URL query. Defaults to `false`.
        #
        #            This is similar to ClearURLs"s **"Allow referral marketing"** option.
        #
        #        ignoreRules (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to not remove tracking fields from the given URL. Defaults to `false`.
        #
        #        ignoreExceptions (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to ignore exceptions for the given URL. Defaults to `false`.
        #
        #        ignoreRawRules (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to ignore raw rules for the given URL. Defaults to `false`.
        #
        #        ignoreRedirections (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to ignore redirection rules for the given URL. Defaults to `false`.
        #
        #        skipBlocked (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to not process rules for blocked URLs. Defaults to `false`.
        #
        #            This is similar to ClearURLs **"Allow domain blocking"** option, but instead of blocking access to the URL,
        #            we just ignore all rulesets where it"s blocked.
        #
        #        stripDuplicates (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to strip fields with duplicate names. Defaults to `false`.
        #
        #        stripEmpty (`Bool` | **optional**):
        #            Pass `true` to instruct Unalix to strip fields with empty values. Defaults to `false`.
        #
        #    Usage examples:
        #
        #      Common rules (used to remove tracking fields found in the URL query)
        #
        #          ```
        #            require "unalix"
        #
        #            url : String = "https://deezer.com/track/891177062?utm_source=deezer"
        #            Unalix.clear_url(url: url) # => "https://deezer.com/track/891177062"
        #          ```
        #
        #      Redirection rules (used to extract redirect URLs found in any part of the URL)
        #
        #          ```
        #            require "unalix"
        #
        #            url : String = "https://www.google.com/url?q=https://pypi.org/project/Unalix"
        #            Unalix.clear_url(url: url) # => "https://pypi.org/project/Unalix"
        #          ```
        #
        #      Raw rules (used to remove tracking elements found in any part of the URL)
        #
        #          ```
        #            require "unalix"
        #
        #            url : String = "https://www.amazon.com/gp/B08CH7RHDP/ref=as_li_ss_tl"
        #            Unalix.clear_url(url: url) # => "https://www.amazon.com/gp/B08CH7RHDP"
        #          ```
        #
        #      Referral marketing rules (used to remove referral marketing fields found in the URL query)
        #
        #          ```
        #            require "unalix"
        #
        #            url : String = "https://natura.com.br/p/2458?consultoria=promotop"
        #            Unalix.clear_url(url: url) # => "https://natura.com.br/p/2458"
        #          ```
        #

        uri : URI = URI.parse(url.to_s)
        exception_matched : Bool

        RULESETS.each do |ruleset|

            if skipBlocked && ruleset.completeProvider
                next
            end

            # https://docs.clearurls.xyz/latest/specs/rules/#urlpattern
            if ruleset.urlPattern.match("#{uri.scheme}://#{uri.host}")
                unless ignoreExceptions
                    exception_matched = false
                    # https://docs.clearurls.xyz/latest/specs/rules/#exceptions
                    ruleset.exceptions.each do |exception|
                        if exception.match(uri.to_s)
                            exception_matched = true
                            break
                        end
                    end
                    if exception_matched
                        next
                    end
                end

                unless ignoreRedirections
                    # https://docs.clearurls.xyz/latest/specs/rules/#redirections
                    ruleset.redirections.each do |redirection|

                        unless redirection.match(uri.to_s)
                            next
                        end

                        # Skip empty URLs
                        if $1 == ""
                            next
                        end

                        if $1 == uri.to_s
                            next
                        end

                        uri = URI.parse(Unalix.requote_uri(URI.decode($1)))

                        # Workaround for URLs without scheme (see https://github.com/ClearURLs/Addon/issues/71)
                        if uri.scheme.nil?
                            uri = URI.parse("http://" + uri.to_s)
                        end

                        return clear_url(
                            url: uri.to_s,
                            ignoreReferralMarketing: ignoreReferralMarketing,
                            ignoreRules: ignoreRules,
                            ignoreExceptions: ignoreExceptions,
                            ignoreRawRules: ignoreRawRules,
                            ignoreRedirections: ignoreRedirections,
                            skipBlocked: skipBlocked,
                            stripDuplicates: stripDuplicates,
                            stripEmpty: stripEmpty
                        )
                    end
                end

                if uri.query && uri.query != ""
                    unless ignoreRules
                        # https://docs.clearurls.xyz/latest/specs/rules/#rules
                        ruleset.rules.each do |rule|
                            uri.query = uri.query.to_s.gsub(pattern: rule, replacement: "\\1")
                        end
                    end

                    unless ignoreReferralMarketing
                        # https://docs.clearurls.xyz/latest/specs/rules/#referralmarketing
                        ruleset.referralMarketing.each do |referral|
                            uri.query = uri.query.to_s.gsub(pattern: referral, replacement: "\\1")
                        end
                    end
                end

                # The fragment might contains tracking fields as well
                if uri.fragment && uri.fragment != ""
                    unless ignoreRules
                        ruleset.rules.each do |rule|
                            uri.fragment = uri.fragment.to_s.gsub(pattern: rule, replacement: "\\1")
                        end
                    end
                    unless ignoreReferralMarketing
                        ruleset.referralMarketing.each do |referral|
                            uri.fragment = uri.fragment.to_s.gsub(pattern: referral, replacement: "\\1")
                        end
                    end
                end

                if uri.path && uri.path != ""
                    unless ignoreRawRules
                        # https://docs.clearurls.xyz/latest/specs/rules/#rawrules
                        ruleset.rawRules.each do |rawRule|
                            uri.path = uri.path.to_s.gsub(pattern: rawRule, replacement: "")
                        end
                    end
                end
            end
        end

        if uri.query
            uri.query = filter_query(
                query: uri.query.to_s,
                stripDuplicates: stripDuplicates,
                stripEmpty: stripEmpty
            )
        end

        if uri.fragment
            uri.query = filter_query(
                query: uri.fragment.to_s,
                stripDuplicates: stripDuplicates,
                stripEmpty: stripEmpty
            )
        end

        if uri.query && uri.query == ""
            uri.query = nil
        end

        if uri.fragment && uri.fragment == ""
            uri.fragment = nil
        end

        return uri.to_s

    end
end
